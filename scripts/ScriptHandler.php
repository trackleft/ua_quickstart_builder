<?php

/**
 * @file
 * Contains \UaDrupal\UaQuickStart\ScriptHandler.
 */

namespace UaDrupal\UaQuickStart;

use Composer\Script\Event;
use Composer\Util\ProcessExecutor;

class ScriptHandler {

  /**
   * Moves front-end libraries to UA QuickStart's installed directory.
   *
   * @param \Composer\Script\Event $event
   *   The script event.
   */
  public static function deployLibraries(Event $event) {
    $extra = $event->getComposer()->getPackage()->getExtra();

    if (isset($extra['installer-paths'])) {
      foreach ($extra['installer-paths'] as $path => $criteria) {
        if (array_intersect(['ua_drupal/ua_quickstart', 'type:drupal-profile'], $criteria)) {
          $ua_quickstart = $path;
        }
      }
      if (isset($ua_quickstart)) {
        $ua_quickstart = str_replace('{$name}', 'ua_quickstart', $ua_quickstart);

        $executor = new ProcessExecutor($event->getIO());
        $output = NULL;
        $executor->execute('npm run install-libraries', $output, $ua_quickstart);
      }
    }
  }

}
